from django import forms
from .models import Message

class Message_Form(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['judulProyek','tanggal', 'deskripsi', 'tempat', 'kategori']

    # attrs = {
    #     'class' : 'form-control'
    # }

    # judulProyek = forms.CharField(label='Judul Proyek :', required=False,
    #     max_length=27, empty_value='Anonymous',
    #     widget=forms.TextInput(attrs=attrs))
    # tanggal = forms.CharField(label='Tanggal mulai - tanggal selesai:', required=False,
    #     max_length=27, empty_value='Anonymous',
    #     widget=forms.TextInput(attrs=attrs))
    # deskripsi = forms.CharField(label='Deskripsi Singkat Proyek', required=False,
    #     max_length=27, empty_value='Anonymous',
    #     widget=forms.TextInput(attrs=attrs))
    # tempat = forms.CharField(label='Tempat', required=False,
    #     max_length=27, empty_value='Anonymous',
    #     widget=forms.TextInput(attrs=attrs))
    # kategori = forms.CharField(label='Kategori Proyek', required=False,
    #     max_length=27, empty_value='Anonymous',
    #     widget=forms.TextInput(attrs=attrs))