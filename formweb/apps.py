from django.apps import AppConfig


class FormwebConfig(AppConfig):
    name = 'formweb'
