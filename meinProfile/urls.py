from django.urls import include, path
from .views import landingPage, bakat, riwayat, hobi
urlpatterns = [
    path('', landingPage, name='landingPage'),
    path('bakat/', bakat, name='bakat'),
    path('riwayat/', riwayat, name='riwayat'),
    path('hobi/', hobi, name='hobi')
]